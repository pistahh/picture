#!/bin/bash

PICTUREDIR=/opt/cam
LOGNAME="deletepictures"

if (( $(df --output=pcent $PICTUREDIR | tr -d '%' | tail -1)  > 80 )); then
    logger -t $LOGNAME "We have to delete"
    cd $PICTUREDIR
    ls -t $PICTUREDIR | grep "vaci.*jpg" | tail -10000 | tee -a rmlog.txt | xargs rm
    logger -t $LOGNAME "Remaining files $(find $PICTUREDIR -type f -name "*jpg" | wc -l)"
else
    logger -t $LOGNAME "Dont have to delete"
fi

