#!/home/pi/picture/venv/bin/python3
from astral.geocoder import database, lookup
from astral.location import Location
if Location(lookup("Budapest", database())).solar_elevation(observer_elevation=22) > -3:
    print("beach")
else:
    print("night")
