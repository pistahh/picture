from flask import Flask, url_for, send_file, render_template
from glob import glob
from os.path import getmtime
app = Flask(__name__)

@app.route("/")
@app.route("/<int:number>")
def hello(number =  0):
    pictures = glob('/opt/cam/vaci*jpg')
    return render_template('index.html', 
            number = number, 
            maxnumber = len(pictures) - 1)

@app.route("/pic/<int:number>")
def pic(number):
    pictures = sorted(glob('/opt/cam/vaci*jpg'), key=getmtime, reverse=True)
    if number >= len(pictures):
        number = len(pictures) - 1
    return send_file(pictures[number], cache_timeout = -1)


if __name__ == "__main__":
    app.run(host='0.0.0.0')
